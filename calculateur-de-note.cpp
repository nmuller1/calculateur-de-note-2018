/*
 author = Noëmie Muller
 matricule = 000458865
 date = 16/02/2018
 cours = info_f_105
 */

#include <iostream>

/*
 * \brief  Fonction qui demande à l'utilisateur sa note pour l'examen demandé
 * \param  None
 * \return cote  La note de l'examen
 */
int askResult()
{
    int cote(0);
    std::cin >> cote;
    while (0 > cote || cote > 20)
    {
        std::cout << "La note doit être un entier compris entre 0 et 20. Veuillez entrer votre note : " ;
        std::cin >> cote;
    }
    return cote;
}

/*
 * \brief  Fonction qui double le poids de la note si echec
 * \param  note, poids  La note et son poids
 * \return None         L'appel par référence permet de modifier directement les poids
 */
void checkPoids(double note, int& poids)
{
    if (note < 10)
    {
        poids *= 2;
    }
}

/*
 * \brief  Fonction qui calcule la cote finale du cours selon les input de l'utilisateur
 * \param  None
 * \return None
 */
void noteCalc()
{
    std::cout << "Bonjour et bienvenue dans noteCalc !" << std::endl;
    int noteProjets(0), noteEcrit(0), noteOral(0);
    for(int i=1;i<=3;i++) //Boucle qui demande à l'utilisateur ses notes pour les 3 projets et les additionne
    {
        std::cout << "Veuillez entrer la note pour le projet " << i << " : " ;
        noteProjets += askResult();
    }
    double moyenneProjets(noteProjets/3); //Calcul de la moyenne des projets
    std::cout << "La moyenne pour les projets est de : " << moyenneProjets << std::endl;
    std::cout << "Veuillez entrer la note pour l'examen écrit : ";
    noteEcrit = askResult();
    std::cout << "Veuillez entrer la note pour l'examen oral : ";
    noteOral = askResult();
    
    int poidsProjets(2), poidsEcrit(3), poidsOral(5) ;
    //Mise à jour des poids selon les résultats entrés par l'utilisateur
    checkPoids(moyenneProjets,poidsProjets);
    checkPoids(noteEcrit,poidsEcrit);
    checkPoids(noteOral,poidsOral);
    int totalPoids(poidsProjets+poidsEcrit+poidsOral);
    //Calcul final de la note
    double noteFinale((moyenneProjets*poidsProjets+noteEcrit*poidsEcrit+noteOral*poidsOral)/totalPoids);
    std::cout << "La note finale est de : " << noteFinale << std::endl;
}

int main()
{
    noteCalc();
    return 0;
}

